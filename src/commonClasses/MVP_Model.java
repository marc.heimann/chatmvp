package commonClasses;

import ChatRoomMVP.ServiceLocator;
import MVP_Message.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.net.Socket;

import ChatRoomMVP.abstractClasses.Model;

/**
 * Copyright 2015, FHNW, Prof. Dr. Brad Richards. All rights reserved. This code
 * is licensed under the terms of the BSD 3-clause license (see the file
 * license.txt).
 *
 * @author Marc Heimann, Vithursan Thayananthan
 */
public class MVP_Model extends Model {

    public static ObservableList<String> room = FXCollections.observableArrayList();
    private ServiceLocator serviceLocator;
    private int value;
    public String ipAddress = "147.86.8.31";
    public int port = 50001;
    private Socket socket;
    public volatile String msg;
    public String token;
    public static SimpleStringProperty newestMessage = new SimpleStringProperty();
    public String oldChatroom;
    public String username;

    public MVP_Model() {
        serviceLocator = ServiceLocator.getServiceLocator();
        value = 0;

        try {
            socket = new Socket(ipAddress, port);
            serviceLocator.getLogger().info("successfully connected");

        } catch (Exception e) {
            serviceLocator.getLogger().warning("connection failed");
        }
        readThread();
        serviceLocator.getLogger().info("Application model initialized");
    }

    public ObservableList<String> getRooms() {

        return room;
    }

    public void readThread() {
        serviceLocator = ServiceLocator.getServiceLocator();
        // Create thread to read incoming messages
        Runnable r = new Runnable() {
            @Override
            public void run() {
                while (true) {
                    msg = Message.receive(socket);
                    // If the client is sending the _same_ message as before, we cannot simply
                    // set the property, because this would not be a change, and the change
                    // listener will not trigger. Therefore, we first erase the previous message.
                    // This is a change, but empty messages are ignored by the change-listener
                    // newestMessage.set(""); // erase previous message
                    // newestMessage.set(msg.getName() + ": " + msg.getContent());
                }
            }
        };
        Thread t = new Thread(r);
        t.setName("Thread to read messages");
        t.setDaemon(true);
        t.start();
    }

    public String receiveMessage() {
        serviceLocator = ServiceLocator.getServiceLocator();
        serviceLocator.getLogger().info("Receive message");
        return null;
    }

    public int getValue() {
        return value;
    }

    public Socket getSocket() {
        return this.socket;
    }

    public int incrementValue() {
        value++;
        serviceLocator.getLogger().info("Application model: value incremented to " + value);
        return value;
    }//

    public void sendCreateLogin(String name, String password) {
        serviceLocator = ServiceLocator.getServiceLocator();
        Message message = new CreateLogin(name, password);
        message.send(socket);
        serviceLocator.getLogger().info("Send message");
    }

    public void sendLogin(String name, String password) {
        serviceLocator = ServiceLocator.getServiceLocator();
        Message message = new Login(name, password);
        message.send(socket);
        serviceLocator.getLogger().info("Send message");
    }

    public void sendListChatrooms() {
        serviceLocator = ServiceLocator.getServiceLocator();
        Message message = new ListChatrooms(MVP_Client.token);
        message.send(socket);
        serviceLocator.getLogger().info("Send message");
    }

    public void sendCreateChat(String chatroom, boolean isPublic) {
        serviceLocator = ServiceLocator.getServiceLocator();
        Message message = new CreateChatroom(MVP_Client.token, chatroom, isPublic);
        message.send(socket);
        serviceLocator.getLogger().info("Send message");
    }

    public void sendJoinChat(String chatroom) {
        serviceLocator = ServiceLocator.getServiceLocator();
        Message message = new JoinChatroom(MVP_Client.token, chatroom, username);
        message.send(socket);
        serviceLocator.getLogger().info("Send message");
    }

    public void sendMessage(String message) {
        serviceLocator = ServiceLocator.getServiceLocator();

        Message msg = new SendMessage(MVP_Client.token, MVP_Client.chatName, message);
        msg.send(socket);
        serviceLocator.getLogger().info("Send message");
    }

    public void sendLeaveChatroom() {
        serviceLocator = ServiceLocator.getServiceLocator();
        Message msg = new LeaveChatroom(MVP_Client.token, oldChatroom, username);
        msg.send(socket);
        serviceLocator.getLogger().info("Send message");
    }

    public void sendLogout() {
        serviceLocator = ServiceLocator.getServiceLocator();
        Message msg = new Logout(MVP_Client.token);
        MVP_Client.chatName = null;
        MVP_Client.userName = null;
        MVP_Client.token = null;
        msg.send(socket);
        serviceLocator.getLogger().info("Send message");
    }

    public void sendDeleteChatroom(String chatname) {
        serviceLocator = serviceLocator.getServiceLocator();
        Message msg = new DeleteChatroom(MVP_Client.token, chatname);
        msg.send((socket));
        serviceLocator.getLogger().info("Send message");
    }
}
