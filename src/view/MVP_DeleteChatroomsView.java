package view;

import java.util.Locale;
import java.util.Optional;
import java.util.logging.Logger;

import ChatRoomMVP.ServiceLocator;
import commonClasses.MVP_Model;
import commonClasses.Translator;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class MVP_DeleteChatroomsView {


    MVP_Model model;
    Menu menuFile;
    Menu menuFileLanguage;
    Menu menuHelp;

    Alert alert;

    public ListView<String> showChat;
    public Button deleteChatroomButton = new Button();
    public Button backToChatView = new Button();
    public Label deleteSuccess = new Label();

    public MVP_DeleteChatroomsView(MVP_Model model) {
        this.model = model;
        showChat = new ListView<>();
        ServiceLocator.getServiceLocator().getLogger().info("Application view initialized"); // TODO
    }

    public Scene create_GUI() {
        ServiceLocator sl = ServiceLocator.getServiceLocator();
        Logger logger = sl.getLogger();

        MenuBar menuBar = new MenuBar();
        menuFile = new Menu();
        menuFileLanguage = new Menu();
        menuFile.getItems().add(menuFileLanguage);

        for (Locale locale : sl.getLocales()) {
            MenuItem language = new MenuItem(locale.getLanguage());
            menuFileLanguage.getItems().add(language);
            language.setOnAction(event -> {
                sl.getConfiguration().setLocalOption("Language", locale.getLanguage());
                sl.setTranslator(new Translator(locale.getLanguage()));
                updateTexts();
            });
        }

        menuHelp = new Menu();
        menuBar.getMenus().addAll(menuFile, menuHelp);

        BorderPane root = new BorderPane();
        showChat = new ListView<>(model.getRooms());
        VBox buttonContainer = new VBox();
        buttonContainer.getChildren().addAll(deleteChatroomButton, backToChatView);

        deleteChatroomButton.setPrefSize(150,20);
        backToChatView.setPrefSize(150,20);
        root.setCenter(showChat);
        root.setRight(buttonContainer);
        root.setBottom(deleteSuccess);
        root.setTop(menuBar);

        updateTexts();
        Scene scene = new Scene(root);
        scene.getStylesheets().add(
                getClass().getResource("app.css").toExternalForm());
        return scene;
    }

    protected void updateTexts() {
        Translator t = ServiceLocator.getServiceLocator().getTranslator();

        // The menu entries
        menuFile.setText(t.getString("program.menu.file"));
        menuFileLanguage.setText(t.getString("program.menu.file.language"));
        menuHelp.setText(t.getString("program.menu.help"));
        deleteChatroomButton.setText(t.getString("program.button.deleteButton"));
        backToChatView.setText(t.getString("program.button.backButton"));
    }

    public boolean warning() {

        Boolean delete = false;

        alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Delete chatroom!");
        alert.setHeaderText("Do you really want to delete the chatroom?");
        alert.setContentText("After clicking on the button, the chatroom will be deleted automatically");

        ButtonType cancelButtonType = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
        alert.getDialogPane().getButtonTypes().add(cancelButtonType);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            delete = true;
        }

        return delete;

    }

}
