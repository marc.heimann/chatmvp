package view;

import java.util.Locale;
import java.util.logging.Logger;

import ChatRoomMVP.ServiceLocator;
import commonClasses.MVP_Model;
import commonClasses.Translator;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.animation.TranslateTransition;
import javafx.util.Duration;

public class MVP_LoginView {

	Menu menuFile;
	Menu menuFileLanguage;
	Menu menuHelp;
	Menu menuServer;
	public Menu serverSettings;
	public Menu settings;

	HBox hContainer, backContainer;
	public Label accountLabel = new Label();
	public Label registerLabel = new Label();

	// LoginView
	Stage stage;
	MVP_Model model;
	public Button loginButton = new Button("Login");
	public TextField userTextField = new TextField();
	public PasswordField passwordTextField = new PasswordField();
	Label userLabel = new Label();
	Label passwordLabel = new Label();
	public Label newRegisterLabel = new Label();

	// RegistartionView
	public Button registerButton = new Button();
	public Label backLabel = new Label();
	public Label userLabel2;
	public Label passwordLabel2;
	public String userName;
	public String pwd;
	public TextField userTextField2 = new TextField();
	public TextField passwordTextField2 = new TextField();

	public MVP_LoginView(Stage stage, MVP_Model model) {
		this.stage = stage;
		this.model = model;
		this.stage.setScene(create_GUI());
		ServiceLocator.getServiceLocator().getLogger().info("Application view initialized");
	}

	public Scene create_GUI() {
		ServiceLocator sl = ServiceLocator.getServiceLocator();
		Logger logger = sl.getLogger();

		hContainer = new HBox();
		backContainer = new HBox();
		backContainer.getChildren().addAll(backLabel, newRegisterLabel);
		backLabel.setStyle("-fx-underline: true");
		VBox vContainer = new VBox();
		vContainer.getChildren().addAll(accountLabel, registerLabel, backContainer);
		newRegisterLabel.setStyle("-fx-underline: true");
		vContainer.setPadding(new Insets(250, 0, 0, 40));
		registerLabel.setStyle("-fx-text-fill: black");
		accountLabel.setStyle("-fx-text-fill: black");
		hContainer.getChildren().add(vContainer);
		hContainer.setStyle("-fx-background-color: #FCE7CA");
		hContainer.setPrefSize(340, 500);

		MenuBar menuBar = new MenuBar();
		menuFile = new Menu();
		menuFileLanguage = new Menu();
		settings = new Menu();
		menuServer = new Menu("Server");
		serverSettings = new Menu();
		menuFile.getItems().add(menuFileLanguage);
		menuServer.getItems().add(serverSettings);

		ImageView imageView = new ImageView();
		ImageView imageView2 = new ImageView();
		imageView.setImage(new Image("view/image/Logo.png"));
		imageView2.setImage(new Image("view/image/Logo.png"));

		for (Locale locale : sl.getLocales()) {
			MenuItem language = new MenuItem(locale.getLanguage());
			menuFileLanguage.getItems().add(language);
			language.setOnAction(event -> {
				sl.getConfiguration().setLocalOption("Language", locale.getLanguage());
				sl.setTranslator(new Translator(locale.getLanguage()));
				updateTexts();
			});
		}

		menuHelp = new Menu();
		menuBar.getMenus().addAll(menuFile, menuHelp, menuServer);
	
		AnchorPane root = new AnchorPane();
		// Login
		HBox registrationContainer = new HBox();
		VBox labelContainer = new VBox();
		VBox textFieldContainer = new VBox();
		VBox userTextFieldContainer = new VBox();
		VBox passwordTextFieldContainer = new VBox();
		HBox loginContainer = new HBox();
		VBox rootContainer = new VBox();

		labelContainer.getChildren().addAll(userLabel, passwordLabel);
		userTextFieldContainer.getChildren().add(userTextField);
		passwordTextFieldContainer.getChildren().add(passwordTextField);
		textFieldContainer.getChildren().addAll(userTextFieldContainer, passwordTextFieldContainer);
		loginContainer.getChildren().addAll(labelContainer, textFieldContainer)
;		registrationContainer.getChildren().addAll(loginButton);
		loginButton.setPrefWidth(255);
		rootContainer.getChildren().addAll(imageView, loginContainer, loginButton);

		rootContainer.setAlignment(Pos.CENTER);
		labelContainer.setAlignment(Pos.CENTER_RIGHT);
		textFieldContainer.setAlignment(Pos.CENTER_RIGHT);
		registrationContainer.setAlignment(Pos.CENTER_RIGHT);

		userLabel.setPadding(new Insets(0, 0, 10, 0));
		userTextFieldContainer.setPadding(new Insets(0,0,10,0));

		registrationContainer.setSpacing(45);

		// Registration
		VBox labelContainer2 = new VBox();
		VBox textFieldContainer2 = new VBox();
		VBox userTextFieldContainer2 = new VBox();
		VBox passwordTextFieldContainer2 = new VBox();
		HBox registrationContainer2 = new HBox();
		VBox rootContainer2 = new VBox();
		HBox buttonContainer2 = new HBox();

		buttonContainer2.getChildren().add(registerButton);

		userLabel2 = new Label("User ");
		passwordLabel2 = new Label("Password ");
		labelContainer2.getChildren().addAll(userLabel2, passwordLabel2);
		userTextFieldContainer2.getChildren().add(userTextField2);
		passwordTextFieldContainer2.getChildren().add(passwordTextField2);
		textFieldContainer2.getChildren().addAll(userTextFieldContainer2, passwordTextFieldContainer2);
		registrationContainer2.getChildren().addAll(labelContainer2, textFieldContainer2);
		rootContainer2.getChildren().addAll(imageView2, registrationContainer2, buttonContainer2);
		rootContainer2.setStyle("-fx-background-color:#EDD8BC");

		labelContainer2.setAlignment(Pos.CENTER_RIGHT);
		textFieldContainer2.setAlignment(Pos.CENTER_RIGHT);
		rootContainer2.setAlignment(Pos.CENTER);
		buttonContainer2.setAlignment(Pos.CENTER_RIGHT);

		registerButton.setPrefWidth(255);
		userLabel2.setPadding(new Insets(0, 0, 10, 0));
		userTextFieldContainer2.setPadding(new Insets(0,0,10,0));
		buttonContainer2.setSpacing(45);

		root.setLeftAnchor(rootContainer2, 25.0);
		root.setTopAnchor(rootContainer2, 80.0);
		root.setRightAnchor(rootContainer, 25.0);
		root.setTopAnchor(rootContainer, 80.0);
		root.setLeftAnchor(hContainer, 0.0);
		root.setTopAnchor(hContainer, 29.0);

		root.getChildren().addAll(rootContainer, rootContainer2, menuBar, hContainer);

		updateTexts();
		Scene scene = new Scene(root, 680, 500);
		menuBar.setPrefWidth(scene.getWidth());
		scene.getStylesheets().add(getClass().getResource("app.css").toExternalForm());
		return scene;
	}

	protected void updateTexts() {
		Translator t = ServiceLocator.getServiceLocator().getTranslator();

		// The menu entries
		menuFile.setText(t.getString("program.menu.file"));
		menuFileLanguage.setText(t.getString("program.menu.file.language"));
		menuHelp.setText(t.getString("program.menu.help"));
		serverSettings.setText(t.getString("program.menu.server"));
		settings.setText(t.getString("program.menu.settings"));
		newRegisterLabel.setText(t.getString("program.label.registerlink"));
		passwordLabel.setText(t.getString("program.label.password")); // TODO
		passwordLabel2.setText(t.getString("program.label.password"));
		userLabel.setText(t.getString("program.label.username")); // TODO
		userLabel2.setText(t.getString("program.label.username"));
		registerButton.setText(t.getString("program.button.registerbutton"));
		accountLabel.setText(t.getString("program.label.newAccount"));
		registerLabel.setText(t.getString("program.label.newRegister"));
		
		// Other controls
		stage.setTitle(t.getString("program.name"));
	}

	public void start() {
		stage.setTitle("MVP Chat Client");
		stage.show();
	}
	
	public void initialize() {
		TranslateTransition translate = new TranslateTransition(Duration.seconds(0.6), hContainer);
		translate.setToX(hContainer.getLayoutX() + (680 - 340));
		translate.play();
	}
	
	public void initialize2() {
		TranslateTransition translate = new TranslateTransition(Duration.seconds(0.6), hContainer);
		translate.setToX(hContainer.getLayoutX());
		translate.play();
	}

	/**
	 * Hide the view
	 */
	public void stop() {
		stage.hide();
	}

	/**
	 * Getter for the stage, so that the controller can access window events
	 */
	public Stage getStage() {
		return stage;
	}

}
