package view;

import java.time.Duration;
import java.util.Locale;
import java.util.Optional;
import java.util.logging.Logger;

import ChatRoomMVP.ServiceLocator;
import commonClasses.MVP_Model;
import commonClasses.Translator;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextFlow;

import javax.management.Notification;

/**
 * Copyright 2015, FHNW, Prof. Dr. Brad Richards. All rights reserved. This code
 * is licensed under the terms of the BSD 3-clause license (see the file
 * license.txt).
 *
 * @author Brad Richards
 */
public class MVP_ChatView {

	MVP_Model model;
	Menu menuFile;
	Menu menuFileLanguage;
	Menu menuHelp;
	public Menu menuLogout = new Menu();

	Alert alert;

	public ListView<String> showChat;
	public Button addChat = new Button("+");
	public Button deleteChat = new Button("-");

	public Button showUsers = new Button("showUsers"); //TODO
	public ListView<String> showUsersList;

	public TextArea chatWindow = new TextArea();
	public Button sendButton = new Button("Send");
	public TextField messageField = new TextField();
	public Label joinedChat = new Label();
	public Button chatRoomUpdate = new Button("Update");

	public MVP_ChatView(MVP_Model model) {
		this.model = model;
		showChat = new ListView<>();
		showUsersList = new ListView<>();
		ServiceLocator.getServiceLocator().getLogger().info("Application view initialized"); // TODO
	}

	public Scene create_GUI() {
		ServiceLocator sl = ServiceLocator.getServiceLocator();
		Logger logger = sl.getLogger();

		MenuBar menuBar = new MenuBar();
		menuFile = new Menu();
		menuFileLanguage = new Menu();

		menuFile.getItems().addAll(menuFileLanguage, menuLogout);

		for (Locale locale : sl.getLocales()) {
			MenuItem language = new MenuItem(locale.getLanguage());
			menuFileLanguage.getItems().add(language);
			language.setOnAction(event -> {
				sl.getConfiguration().setLocalOption("Language", locale.getLanguage());
				sl.setTranslator(new Translator(locale.getLanguage()));
				updateTexts();
			});
		}

		menuHelp = new Menu();
		menuBar.getMenus().addAll(menuFile, menuHelp);

		BorderPane root = new BorderPane();
		showChat.setItems(model.getRooms());

		BorderPane centerContainer = new BorderPane();
		HBox messageFieldContainer = new HBox();
		VBox chatContainer = new VBox();
		BorderPane usersContainer = new BorderPane();
		usersContainer.setCenter(joinedChat);
		usersContainer.setRight(showUsers);
		chatContainer.getChildren().addAll(usersContainer, chatWindow);
		

		VBox leftContainer = new VBox();
		HBox buttonContainer = new HBox();

		buttonContainer.getChildren().addAll(addChat, deleteChat, chatRoomUpdate);
		
		leftContainer.getChildren().addAll(buttonContainer, showChat);

		messageField.setPrefSize(450, 40);
		messageField.setId("messageField");
		sendButton.setPrefSize(70, 40);
		chatWindow.setPrefSize(450, 300);
		chatWindow.setId("textArea");
		chatContainer.setAlignment(Pos.CENTER);
		joinedChat.setId("chatLabel");
		joinedChat.setPadding(new Insets(5,0,7,0));

		messageFieldContainer.getChildren().addAll(messageField, sendButton);
		centerContainer.setTop(chatContainer);
		centerContainer.setBottom(messageFieldContainer);
		root.setTop(menuBar);
		root.setLeft(leftContainer);
		root.setRight(centerContainer);

		updateTexts();
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("app.css").toExternalForm());
		return scene;
	}

	public boolean warning() {

		Boolean delete = false;

		alert = new Alert(Alert.AlertType.WARNING);
		alert.setTitle("Delete chatroom!");
		alert.setHeaderText("Do you really want to delete the chatroom?");
		alert.setContentText("After clicking on the button, the chatroom will be deleted automatically");

		ButtonType cancelButtonType = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		alert.getDialogPane().getButtonTypes().add(cancelButtonType);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.isPresent() && result.get() == ButtonType.OK) {
			delete = true;
		}
		return delete;
	}

	public void notifications(){

	}

	public boolean logout() {
		boolean logout = false;

		alert = new Alert(Alert.AlertType.WARNING);
		alert.setTitle("Logout!");
		alert.setHeaderText("Do you really want to Logout?");
		alert.setContentText("After clicking on the button, you will logged out");
		ButtonType cancelButtonType = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
		alert.getDialogPane().getButtonTypes().add(cancelButtonType);
		Optional<ButtonType> result = alert.showAndWait();

		if (result.get() == ButtonType.OK) {
			logout = true;
		}
		return logout;
	}

	protected void updateTexts() {
		Translator t = ServiceLocator.getServiceLocator().getTranslator();

		// The menu entries
		menuFile.setText(t.getString("program.menu.file"));
		menuFileLanguage.setText(t.getString("program.menu.file.language"));
		menuHelp.setText(t.getString("program.menu.help"));
		menuLogout.setText(t.getString("program.menu.logout"));
		// alert.setTitle(t.getString("program.alert.title"));
		// alert.setHeaderText(t.getString("program.alert.header"));
		// alert.setContentText(t.getString("program.alert.content"));

	}

}