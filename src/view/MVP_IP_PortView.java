package view;

import ChatRoomMVP.ServiceLocator;
import commonClasses.Translator;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.Locale;
import java.util.logging.Logger;

public class MVP_IP_PortView {

    Menu menuFile;
    Menu menuFileLanguage;
    Menu menuHelp;

    public Button saveButton = new Button();
    public Button backButton = new Button();
    public TextField ipTextField = new TextField();
    public TextField portTextField = new TextField();

    public MVP_IP_PortView() {
        ServiceLocator.getServiceLocator().getLogger().info("Application view initialized");
    }

    public Scene create_GUI() {
        ServiceLocator sl = ServiceLocator.getServiceLocator();
        Logger logger = sl.getLogger();

        MenuBar menuBar = new MenuBar();
        menuFile = new Menu();
        menuFileLanguage = new Menu();
        menuFile.getItems().add(menuFileLanguage);

        for (Locale locale : sl.getLocales()) {
            MenuItem language = new MenuItem(locale.getLanguage());
            menuFileLanguage.getItems().add(language);
            language.setOnAction(event -> {
                sl.getConfiguration().setLocalOption("Language", locale.getLanguage());
                sl.setTranslator(new Translator(locale.getLanguage()));
                updateTexts();
            });
        }

        menuHelp = new Menu();
        menuBar.getMenus().addAll(menuFile, menuHelp);

        BorderPane root = new BorderPane();
        HBox registrationContainer = new HBox();
        HBox ipContainer = new HBox();
        HBox portContainer = new HBox();
        VBox rootContainer = new VBox();

        Label ipLabel = new Label("IP: ");
        ipContainer.getChildren().addAll(ipLabel, ipTextField);
        Label portLabel = new Label("Port: ");
        portContainer.getChildren().addAll(portLabel, portTextField);

        registrationContainer.getChildren().addAll(backButton, saveButton);
        rootContainer.getChildren().addAll(ipContainer, portContainer, registrationContainer);

        rootContainer.setPrefSize(300, 300);
        ipContainer.setAlignment(Pos.CENTER);
        ipContainer.setPadding(new Insets(5, 0, 5, 0));
        portContainer.setAlignment(Pos.CENTER);
        portContainer.setPadding(new Insets(5, 0, 5, 0));
        rootContainer.setAlignment(Pos.CENTER);

        root.setTop(menuBar);
        root.setCenter(rootContainer);

        updateTexts();
        Scene scene = new Scene(root);
        scene.getStylesheets().add(
                getClass().getResource("app.css").toExternalForm());
        return scene;
    }

    protected void updateTexts() {
        Translator t = ServiceLocator.getServiceLocator().getTranslator();

        // The menu entries
        menuFile.setText(t.getString("program.menu.file"));
        menuFileLanguage.setText(t.getString("program.menu.file.language"));
        menuHelp.setText(t.getString("program.menu.help"));
        saveButton.setText(t.getString("program.button.saveButton"));
        backButton.setText(t.getString("program.button.backButton"));
    }
}