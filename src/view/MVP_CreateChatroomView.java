package view;

import java.util.Locale;
import java.util.logging.Logger;

import ChatRoomMVP.ServiceLocator;
import commonClasses.Translator;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class MVP_CreateChatroomView {
	
	Menu menuFile;
	Menu menuFileLanguage;
	Menu menuHelp;
	
	public TextField createChatroomTextField = new TextField();
	public Button createChatroomButton = new Button();
	public Button backToChatView = new Button();
	
	
	public MVP_CreateChatroomView() {
		ServiceLocator.getServiceLocator().getLogger().info("Application view initialized"); // TODO
	}

	public Scene create_GUI() {

		ServiceLocator sl = ServiceLocator.getServiceLocator();
		Logger logger = sl.getLogger();

		MenuBar menuBar = new MenuBar();
		menuFile = new Menu();
		menuFileLanguage = new Menu();
		menuFile.getItems().add(menuFileLanguage);

		for (Locale locale : sl.getLocales()) {
			MenuItem language = new MenuItem(locale.getLanguage());
			menuFileLanguage.getItems().add(language);
			language.setOnAction(event -> {
				sl.getConfiguration().setLocalOption("Language", locale.getLanguage());
				sl.setTranslator(new Translator(locale.getLanguage()));
				updateTexts();
			});
		}

		menuHelp = new Menu();
		menuBar.getMenus().addAll(menuFile, menuHelp);

		Label creatChatRoomLabel = new Label("Chatname: ");
		
		BorderPane root = new BorderPane();
		HBox hContainer = new HBox();
		HBox buttonContainer = new HBox();
		HBox backToChatContainer = new HBox();
		HBox createChatroomContainer = new HBox();
		VBox vContainer = new VBox();

		hContainer.getChildren().addAll(creatChatRoomLabel, createChatroomTextField);
		backToChatContainer.getChildren().add(backToChatView);
		createChatroomContainer.getChildren().add(createChatroomButton);
		buttonContainer.getChildren().addAll(backToChatContainer, createChatroomContainer);
		vContainer.getChildren().addAll(hContainer, buttonContainer);

		root.setPrefSize(300, 300);
		hContainer.setAlignment(Pos.CENTER);
		vContainer.setAlignment(Pos.CENTER);
		buttonContainer.setAlignment(Pos.CENTER);
		backToChatContainer.setPadding(new Insets(0,2.5,0,0));
		createChatroomContainer.setPadding(new Insets(0,0,0,2.5));
		buttonContainer.setPadding(new Insets(10,0,0,0));
		root.setTop(menuBar);
		root.setCenter(vContainer);
		

		updateTexts();
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("app.css").toExternalForm());

		return scene;
	}

	private void updateTexts() {
		Translator t = ServiceLocator.getServiceLocator().getTranslator();

		// The menu entries
		menuFile.setText(t.getString("program.menu.file"));
		menuFileLanguage.setText(t.getString("program.menu.file.language"));
		menuHelp.setText(t.getString("program.menu.help"));
		createChatroomButton.setText(t.getString("program.button.createButton"));
		backToChatView.setText(t.getString("program.button.backButton"));

	}

}
