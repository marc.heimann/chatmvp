package MVP_Message;


public class UserOnline extends Message {

	private String token;
	private String user;

	public UserOnline(String token, String user) {
		super(MessageType.UserOnline);
		this.token = token;
		this.user = user;
	}

	public String getToken() {
		return this.token;
	}

	public String getUser() {
		return this.user;
	}

	public String toString() {
		return type.toString() + "|" + token + "|" + user;
	}
}
