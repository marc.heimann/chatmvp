package MVP_Message;

public class CreateLogin extends Message{
	
	private String username;
	private String password;

	public CreateLogin(String username, String password) {
		super(MessageType.CreateLogin);
		this.username = username;
		this.password = password;
	}
	
	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public String toString() {
		return type.toString() + '|' + username + '|' + password;
	}
}
