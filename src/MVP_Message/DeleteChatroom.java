package MVP_Message;


public class DeleteChatroom extends Message {
	private String token;
	private String chatroom;
	
	public DeleteChatroom(String token, String chatroom) {
		super(MessageType.DeleteChatroom);
		this.token = token;
		this.chatroom = chatroom;
	}
	
	public String getToken() {
		return this.token;
	}
	
	public String getChatroom() {
		return this.chatroom;
	}
	
	public String toString() {
		return type.toString() + "|" + token + "|" + chatroom;
	}
}
