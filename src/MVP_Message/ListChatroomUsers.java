package MVP_Message;

public class ListChatroomUsers extends Message {
	private String token;
	private String chatroom;
	
	public ListChatroomUsers(String token, String chatroom) {
		super(MessageType.ListChatroomUsers);
		this.token = token;
		this.chatroom = chatroom;
	}
	
	public String getToken() {
		return this.token;
	}
	
	public String getChatroom() {
		return this.chatroom;
	}
	
	public String toString() {
		return type.toString() + "|" + token + "|" + chatroom;
	}
}
