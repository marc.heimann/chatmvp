package MVP_Message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Logger;

import ChatRoomMVP.ServiceLocator;
import commonClasses.MVP_Client;
import commonClasses.MVP_Model;
import javafx.application.Platform;

public abstract class Message {

	private static Logger logger = Logger.getLogger("");
	protected MessageType type;

	public Message(MessageType type) {
		this.type = type;
	}

	public void send(Socket socket) {
		OutputStreamWriter out;
		ServiceLocator serviceLocator = ServiceLocator.getServiceLocator();
		try {
			out = new OutputStreamWriter(socket.getOutputStream());
			serviceLocator.getLogger().info("Sending message: " + this.toString());
			out.write(this.toString() + "\n");
			out.flush();
		} catch (IOException e) {
			serviceLocator.getLogger().warning(e.toString());
		}
	}

	// TODO Am programmieren
	public static String receive(Socket socket) {
		ServiceLocator serviceLocator = ServiceLocator.getServiceLocator();
		BufferedReader in;
		String msg = null;
		try {
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String msgText = in.readLine(); // Will wait here for complete line
			logger.info("Receiving message: " + msgText);

			// Break message into individual parts, and remove extra spaces
			String[] parts = msgText.split("\\|");
			for (int i = 0; i < parts.length; i++) {
				parts[i] = parts[i].trim();
			}

			if (parts[1].equals(MessageType.Login.toString())) {
				msg = parts[2];
				boolean a = Boolean.parseBoolean(msg);
				if(a) {
					MVP_Client c = new MVP_Client(parts[3]);
				}
			}
			if (parts[1].equals(MessageType.CreateLogin.toString())) {
				msg = parts[2];
			}
			 if (parts[1].equals(MessageType.ListChatrooms.toString())) {
				//hilfe erhalten von Maksym Kutalo & Lea Peier
				 
					Platform.runLater(new Runnable() {
					    @Override
					    public void run() {
					    	String[] room = new  String[parts.length-3];
							for (int i = 0; i < parts.length-3;i++ ) {
								room[i] = parts[i+3];
							}
							for(int s = 0; s < MVP_Model.room.size();s++) {
								for (int a = 0; a < room.length; a++) {
									if(MVP_Model.room.get(s).equals(room[a])) {
										MVP_Model.room.remove(s);
									}
								}
							}
							MVP_Model.room.addAll(room);
					    }
					});
			 }
			 if(parts[0].equals(MessageType.MessageText.toString())) {
				 Platform.runLater(new Runnable() {
					    @Override
					    public void run() {
				 MVP_Model.newestMessage.set(parts[1]+":"+" " +parts[3]);
					    }
				 });
			 }
			 if(parts[1].equals(MessageType.JoinChatroom.toString())) {
				 msg = parts[2];
					boolean answer2 = Boolean.parseBoolean(msg);
					if(answer2 == false) {
						MVP_Client.chatName = null;
					}
			 }
			 if(parts[1].equals(MessageType.LeaveChatroom.toString())) {
				 msg = parts[2];
				 MVP_Client.chatName = null;
			 }
			 if(parts[1].equals(MessageType.Logout.toString())) {
				 msg = parts[2];
			 }
			 if(parts[1].equals(MessageType.CreateChatroom.toString())) {
				 msg = parts[2];
			 }
			 if(parts[1].equals(MessageType.DeleteChatroom.toString())) {
				 msg = parts[2];
			 }
				
			
			String messageClassName = Message.class.getPackage().getName() + "." + parts[0];

		} catch (

		IOException e) {
			logger.warning(e.toString());

		}
		return msg;
	}

	public MessageType getType() {
		return type;
	}
}
