package MVP_Message;


public class Logout extends Message {

private String token;
	
	public Logout(String token) {
		super(MessageType.Logout);
		this.token = token;
		
	}

	public String getToken() {
		return this.token;
	}
	
	public String toString() {
		return type.toString() + "|" + token;
	}
}
