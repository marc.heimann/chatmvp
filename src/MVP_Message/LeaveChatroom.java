package MVP_Message;

public class LeaveChatroom extends Message {
	private String token;
	private String chatroom;
	private String user;
	
	public LeaveChatroom(String token, String chatroom, String user) {
		super(MessageType.LeaveChatroom);
		this.token = token;
		this.chatroom = chatroom;
		this.user = user;
	}
	
	public String getToken() {
		return this.token;
	}
	
	public String getChatroom() {
		return this.chatroom;
	}
	
	public String getUser() {
		return this.user;
	}
	
	public String toString() {
		return type.toString() + "|" + token + "|" + chatroom + "|" + user;
	}
}
