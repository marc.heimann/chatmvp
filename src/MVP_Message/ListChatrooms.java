package MVP_Message;

public class ListChatrooms extends Message {
private String token;
	
	public ListChatrooms(String token) {
		super(MessageType.ListChatrooms);
		this.token = token;
	}
	
	public String getToken() {
		return this.token;
	}
	
	public String toString() {
		return type.toString() + "|" + token;
	}
}
