package MVP_Message;


public class Login extends Message{
	private String username;
	private String password;
	
	public Login(String username, String password) {
		super(MessageType.Login);
		this.username = username;
		this.password = password;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public String toString() {
		return type.toString() + "|" + username + "|" + password;
	}

}
