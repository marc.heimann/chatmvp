package MVP_Message;


public class CreateChatroom extends Message {
	private String chatroom;
	private boolean isPublic;
	private String token;
	
	public CreateChatroom(String token, String chatroom, boolean isPublic) {
		super(MessageType.CreateChatroom);
		this.token = token;
		this.chatroom = chatroom;
		this.isPublic = isPublic;
	}
	
	public String getToken() {
		return this.token;
	}

	public String getChatroom() {
		return this.chatroom;
	}
	
	public boolean getIsPublic() {
		return this.isPublic;
	}
	
	public String toString() {
		return type.toString() + "|" + token + "|" + chatroom + "|" + isPublic;
	}

}
