package MVP_Message;

public class Ping extends Message {
	private String token;

	public Ping(String token) {
		super(MessageType.Ping);
		this.token = token;
	}

	public Ping() {
		super(MessageType.Ping);
	}

	public String getToken() {
		return this.token;
	}

	public String toString() {
		return type.toString() + "|" + token;
	}
}
