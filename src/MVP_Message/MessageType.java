package MVP_Message;

public enum MessageType {
    CreateLogin, Login, Logout, Chat, Join, DeleteLogin, CreateChatroom,
    JoinChatroom, LeaveChatroom, DeleteChatroom, ListChatrooms, Ping, SendMessage,
    MessageText, UserOnline, ListChatroomUsers, ChangePassword;
}
