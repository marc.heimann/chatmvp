package MVP_Message;

public class SendMessage extends Message {
	
	private String token;
	private String target;
	private String message;
	
	public SendMessage(String token, String target, String message) {
		super(MessageType.SendMessage);
		this.token = token;
		this.target = target;
		this.message = message;
	}
	
	public String getToken() {
		return this.token;
	}
	
	public String getTarget() {
		return this.target;
	}
	
	public String getMessage() {
		return this.message;
	}
	
	public String toString() {
		return type.toString() + "|" + token + "|" + target + "|" + message;
	}

}