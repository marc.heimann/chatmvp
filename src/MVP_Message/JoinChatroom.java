package MVP_Message;

public class JoinChatroom extends Message {
	private String token;
	private String chatroom;
	private String username;
	
	public JoinChatroom(String token, String chatroom, String username) {
		super(MessageType.JoinChatroom);
		this.token = token;
		this.chatroom = chatroom;
		this.username = username;
	}
	
	
	public String getToken() {
		return this.token;
	}
	
	public String getChatroom() {
		return this.chatroom;
	}
	
	public String getUser() {
		return this.username;
	}
	
	public String toString() {
		return type.toString() + "|" + token + "|" + chatroom + "|" + username;
	}

}

