package controller;

import ChatRoomMVP.ServiceLocator;
import commonClasses.MVP_Client;
import commonClasses.MVP_Model;
import commonClasses.MVP_Sound;
import commonClasses.Translator;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import view.MVP_ChatView;
import view.MVP_DeleteChatroomsView;
import view.MVP_CreateChatroomView;
import view.MVP_IP_PortView;
import view.MVP_LoginView;

/**
 * Copyright 2015, FHNW, Prof. Dr. Brad Richards. All rights reserved. This code
 * is licensed under the terms of the BSD 3-clause license (see the file
 * license.txt).
 *
 * @author Brad Richards
 */
public class MVP_Controller {
	ServiceLocator serviceLocator = ServiceLocator.getServiceLocator();
	MVP_LoginView loginView;
	MVP_Model model;
	MVP_ChatView chatView;
	MVP_IP_PortView ip_portView = new MVP_IP_PortView();
	MVP_CreateChatroomView createChatRoom = new MVP_CreateChatroomView();
	MVP_DeleteChatroomsView chatroomView;
	Stage stage;
	Boolean isJoined = false;

	public MVP_Controller(MVP_Model model, MVP_LoginView loginView) {
		this.model = model;
		this.loginView = loginView;

		this.stage = loginView.getStage();
		chatroomView = new MVP_DeleteChatroomsView(model);

		chatView = new MVP_ChatView(model);

		// register ourselves to handle window-closing event
		loginView.getStage().setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				if (MVP_Client.chatName != null) {
					model.sendLeaveChatroom();
					MVP_Client.chatName = null;
					try {
						Thread.sleep(500);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				try {
					Thread.sleep(500);
				} catch (Exception e) {
					e.printStackTrace();
				}
				Platform.exit();
			}
		});

		serviceLocator = ServiceLocator.getServiceLocator();
		serviceLocator.getLogger().info("Application controller initialized");

		loginView.loginButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				model.msg = null;
				String username = loginView.userTextField.getText();
				String password = loginView.passwordTextField.getText();

				model.sendLogin(username, password);
				try {
					Thread.sleep(500);
				} catch (Exception e) {
					e.printStackTrace();
				}
				boolean a = Boolean.parseBoolean(model.msg);
				if (a) {
					model.sendListChatrooms();
					try {
						Thread.sleep(500);
					} catch (Exception e) {
						e.printStackTrace();
					}
					stage.setScene(chatView.create_GUI());
					MVP_Client.userName = username;
					model.msg = null;
				} else {
					ServiceLocator.getServiceLocator().getLogger().info("Login fehlgeschlagen");
				}
			}
		});

		loginView.newRegisterLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Translator t = ServiceLocator.getServiceLocator().getTranslator();
				loginView.accountLabel.setText(t.getString("program.label.haveAccount"));
				loginView.registerLabel.setText(t.getString("program.label.backLogin"));
				loginView.newRegisterLabel.setText("");
				loginView.backLabel.setText(t.getString("program.label.backLabel"));
				loginView.userTextField2.clear();
				loginView.passwordTextField2.clear();
				loginView.initialize();
			}
		});

		loginView.registerButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				model.msg = null;
				String username = loginView.userTextField2.getText();
				String password = loginView.passwordTextField2.getText();
				model.sendCreateLogin(username, password);
				try {
					Thread.sleep(500);
				} catch (Exception e) {
					e.printStackTrace();
				}

				Boolean a = Boolean.parseBoolean(model.msg);
				if (a) {
					Translator t = ServiceLocator.getServiceLocator().getTranslator();
					loginView.userTextField2.clear();
					loginView.passwordTextField2.clear();
					loginView.accountLabel.setText(t.getString("program.label.newAccount"));
					loginView.registerLabel.setText(t.getString("program.label.newRegister"));
					loginView.newRegisterLabel.setText(t.getString("program.label.registerlink"));
					loginView.initialize2();
				} else {
					ServiceLocator.getServiceLocator().getLogger().info("Registration fehlgeschlagen");
				}
			}
		});

		loginView.settings.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				stage.setScene(ip_portView.create_GUI());
			}
		});

		ip_portView.saveButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				stage.setScene(loginView.create_GUI());
			}
		});

		chatView.addChat.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				stage.setScene(createChatRoom.create_GUI());
			}
		});

		chatView.deleteChat.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				stage.setScene(chatroomView.create_GUI());
			}
		});

		chatroomView.backToChatView.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				chatView.chatWindow.clear();
				chatroomView.deleteSuccess.setText("");
				stage.setScene(chatView.create_GUI());
			}
		});

		chatroomView.deleteChatroomButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {

				model.oldChatroom = chatroomView.showChat.getSelectionModel().getSelectedItem();
				model.username = loginView.userTextField.getText();
				if (chatView.warning()) {
					if (MVP_Client.chatName != null) {
						model.sendLeaveChatroom();
						MVP_Client.chatName = null;
						try {
							Thread.sleep(500);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					model.sendDeleteChatroom(model.oldChatroom);
					try {
						Thread.sleep(1000);
					} catch (Exception e) {
						e.printStackTrace();
					}
					Boolean a = Boolean.parseBoolean(model.msg);
					if (a) {
						try {
							Thread.sleep(500);
						} catch (Exception e) {
							e.printStackTrace();
						}
						for (int i = 0; i < MVP_Model.room.size(); i++) {
							if (model.oldChatroom.equals(MVP_Model.room.get(i))) {
								MVP_Model.room.remove(i);
							}
						}
						model.sendListChatrooms();
						try {
							Thread.sleep(500);
						} catch (Exception e) {
							e.printStackTrace();
						}
						chatroomView.deleteSuccess.setText("Delete successfull");
					} else {
						ServiceLocator.getServiceLocator().getLogger().info("löschen nicht möglich");
					}
				} else {
					ServiceLocator.getServiceLocator().getLogger().info("Löschvorgang abgebrochen");
				}
			}
		});

		createChatRoom.backToChatView.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				stage.setScene(chatView.create_GUI());
			}
		});

		createChatRoom.createChatroomButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				String chatroom = createChatRoom.createChatroomTextField.getText();
				model.sendCreateChat(chatroom, true);
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Boolean a = Boolean.parseBoolean(model.msg);
				if (a) {
					MVP_Model.room.add(createChatRoom.createChatroomTextField.getText());
					stage.setScene(chatView.create_GUI());
				} else {
					ServiceLocator.getServiceLocator().getLogger().info("Chatroom erstellen fehlgeschlagen");
				}
			}
		});

		chatView.showChat.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				String joinChatNew = chatView.showChat.getSelectionModel().getSelectedItem();
				chatView.joinedChat.setText("Chatroom: " + joinChatNew);
				model.username = loginView.userTextField.getText();
				if (isJoined) {
					if (MVP_Client.chatName != null) {
						model.sendLeaveChatroom();
						MVP_Client.chatName = null;
						try {
							Thread.sleep(500);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				try {
					Thread.sleep(500);
				} catch (Exception e) {
					e.printStackTrace();
				}
				model.oldChatroom = joinChatNew;
				chatView.chatWindow.clear();
				model.sendJoinChat(joinChatNew);
				try {
					Thread.sleep(500);
				} catch (Exception e) {
					e.printStackTrace();
				}
				MVP_Client.chatName = joinChatNew;
				isJoined = true;
			}
		});

		MVP_Model.newestMessage.addListener(new ChangeListener<String>() {
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.isEmpty()) {
					chatView.chatWindow.appendText(newValue + "\n");
				}
			}
		});

		chatView.sendButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				MVP_Sound.playSound();
				String msg = chatView.messageField.getText();
				String joinChatNew = chatView.showChat.getSelectionModel().getSelectedItem();
				if (MVP_Client.chatName != null && !chatView.messageField.getText().trim().isEmpty()) {
					MVP_Client.chatName = joinChatNew;
					model.sendMessage(msg);
					chatView.messageField.clear();
				} else {
					ServiceLocator.getServiceLocator().getLogger().info("Nachricht senden fehlgeschlagen");
				}
			}
		});

		loginView.backLabel.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Translator t = ServiceLocator.getServiceLocator().getTranslator();
				loginView.accountLabel.setText(t.getString("program.label.newAccount"));
				loginView.registerLabel.setText(t.getString("program.label.newRegister"));
				loginView.newRegisterLabel.setText(t.getString("program.label.registerlink"));
				loginView.backLabel.setText("");
				loginView.userTextField.clear();
				loginView.passwordTextField.clear();
				loginView.initialize2();
			}
		});

		chatView.menuLogout.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				if (chatView.logout()) {
					model.msg = null;
					if (MVP_Client.chatName != null) {
						model.sendLeaveChatroom();
						MVP_Client.chatName = null;
						try {
							Thread.sleep(500);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					model.sendLogout();
					try {
						Thread.sleep(500);
					} catch (Exception e) {
						e.printStackTrace();
					}

					Boolean a = Boolean.parseBoolean(model.msg);
					if (a) {
						loginView.userTextField.clear();
						loginView.passwordTextField.clear();
						stage.setScene(loginView.create_GUI());
					} else {
						ServiceLocator.getServiceLocator().getLogger().info("Logout fehlgeschlagen");
					}
				}
			}
		});

		loginView.serverSettings.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				stage.setScene(ip_portView.create_GUI());
			}
		});

		ip_portView.backButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				stage.setScene(loginView.create_GUI());
			}
		});

		ip_portView.saveButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				model.ipAddress = ip_portView.ipTextField.getText();
				model.port = Integer.parseInt(ip_portView.ipTextField.getText());
			}
		});

		chatView.chatRoomUpdate.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				model.sendListChatrooms();
				try {
					Thread.sleep(500);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}
}
